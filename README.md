ReadMe.txt

This quiz can be run using browser as well as command line

To run this script in browser, please follow the below instructions:

1. Run the project on browser using Apache2 server and PHP version > 5.3
2. Browse the input text file of vine that needs to be uploaded. That file should be text file only with .txt extension.
3. Result will be saved in same directory where the project will be running and you can download the same using a link

To run this script using command line, please follow the below command

php command.php input_file.txt output_file.txt

These are 3 manadatory parameters,
first is name of the file to be executed
second is input file of vine and persons
third is the name of the file you want to take output in