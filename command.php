<?php
require_once("vineyards.php");
if(count($argv) != 3) {
  echo "Wrong Input\n"; die;
}
$input_file = $argv[1];
$output_file = $argv[2];
$objVine = new Vineyards();
$validFile = json_decode($objVine->checkFile($input_file), true);
if(!$validFile['success']) {
  echo $validFile['message']."<br/>";
  die;
}
$objVine->generateWineList($input_file);
$objVine->generateWineAllotmentList();
$validFile = json_decode($objVine->checkFile($output_file), true);
if(!$validFile['success']) {
  $new_output_file = str_replace(".","_",$output_file);
  $output_file = $new_output_file.".txt";
}
$export = $objVine->exportWineAllotmentList($output_file);
echo $export."\nYour output file is '".$output_file."'\n";
