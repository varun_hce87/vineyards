<?php
if(isset($_POST['get_result'])) {

  require_once("vineyards.php");
  $exportFileName = "output.txt";
  $objVine = new Vineyards();
  $validFile = json_decode($objVine->checkFile($_FILES['file']['name']), true);
  if(!$validFile['success']) {
    echo $validFile['message']."<br/>";
    display_form();
    die;
  }
  $objVine->generateWineList($_FILES['file']['tmp_name']);
  $objVine->generateWineAllotmentList();
  $export = $objVine->exportWineAllotmentList($exportFileName);
  echo $export."<br/>Click Here To <a href='".$exportFileName."' download='".$exportFileName."'>Download Result File</a>";
  echo '<br/><a href="">Go Back</a>';
  die;
}
display_form();
function display_form() {
?>

<html>
  <head>
    <title>Vineyards of Apan Problem Solution</title>		
  </head>
  <body>
    <form action="" method="post" enctype="multipart/form-data">
      <div>Select File (Only txt file allow): <input type="file" name="file" accept=".txt" /></div>
      <div><input type="submit" name="get_result" value="Get Result" /></div>
    </form>
  </body>
</html>
<?php
}
?>
