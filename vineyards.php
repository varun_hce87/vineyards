<?php
ini_set('memory_limit', '4095M'); 
set_time_limit(0);
/**
 * vineyards.php is a PHP script which is coded in PHP5.
 * 
 * Class Vineyards contains functions which iterate over the input file and generate the final list of all wines allotement and generate the
 * file with desired result. In this class I have created three methods thats works on three stages of the puzzle.
 *
 * @package    Vineyards Wine Allotment Puzzle
 * @author     Varun Gupta <varun_hce87@yahoo.co.in>
 * @version    1
  */
class Vineyards{
    /**
     * Class Vineyards
     */
	
    public $allWineList;
    public $personWineWishlist;
    public $wineFinalAllotmentList;
    public $totalWineSold;
	
    /**
     * Constructor function 
     *
     * This Function declare the data types of the variables.
     */	 
    public function __construct(){	    
	$this->allWineList            = array();
	$this->personWineWishlist     = array();
	$this->wineFinalAllotmentList = array();
	$this->totalWineSold 	      = 0;
    }
    
    /**
     * This method create the list of all the wines availables in Vineyards of apan and also create list of wishlist of wines.
     *  
     * @param string $wineWishlistFiname Name of filename
     *
     * @return void function is not returning anything
     *
     * @access public
     */	
    public function generateWineList($wineWishlistFileName){
        $fp = fopen($wineWishlistFileName,"r");
	while ( !feof($fp) ){
	    $line = fgets($fp, 2048);
	    $data = str_getcsv($line, "\t"); 
	    if($data[0]!=''){
	        $personName = trim($data[0]);
		$wineCode = trim($data[1]);
		if(!array_key_exists($wineCode, $this->personWineWishlist)){
		    $this->personWineWishlist[$wineCode] = [];
		}
		$this->personWineWishlist[$wineCode][] = $personName;
		$this->allWineList[]=$wineCode;
	    }
	} 
	fclose($fp); 
	$this->allWineList = array_unique($this->allWineList);	 
    }	
	
    /**
     * This method generates the final allotment list of wines to persons.
     *
     * @param
     *
     * @return void
     *
     * @access public
     */
    public function generateWineAllotmentList(){ 
        foreach ($this->allWineList as $key => $wineCode){
            $maxSize = count($wineCode);
            $counter = 0;

            while($counter < 10){
                $i = intval(floatval(rand()/(float)getrandmax()) * $maxSize);
                $personCode = $this->personWineWishlist[$wineCode][$i];
                if(!array_key_exists($personCode, $this->wineFinalAllotmentList)){
                    $this->wineFinalAllotmentList[$personCode] = [];
                }
                if(count($this->wineFinalAllotmentList[$personCode]) < 3) {
                    $this->wineFinalAllotmentList[$personCode][] = $wineCode;
                    $this->totalWineSold++;
                    break;
                }
                $counter++;
            }
        }
    }

    /**
     * This method is used to generate the required tsv file with desired result.
     *
     * @param string $exportFilename Name of the filename that will be saved for exporting data
     *
     * @return string $retStr
     *
     * @access public
     */
    public function exportWineAllotmentList($exportFilename) {
        $fh = fopen($exportFilename, "w");
	$heading="Total number of wines to be sold in aggregate is ".$this->totalWineSold." by vineyards";
	fwrite($fh, $heading );
	foreach ($this->wineFinalAllotmentList as $personCode=>$winelist){
	    foreach ($this->wineFinalAllotmentList[$personCode] as $key => $wineCode){
	        fwrite($fh, "\n".$personCode." \t ".$wineCode);
	    }
	}
	fclose($fh);
	return $heading;
    }

    /**
     * This method is used to check for valid file provided by the user
     *
     * @param string $file Name of the filename that will be checked for proper valid extension
     *
     * @return string
     *
     * @access public
     */
    public function checkFile($file) {
        $arrFile = explode(".", $file);
        if(!empty($arrFile) && count($arrFile) == 2) {
            $filename = $arrFile[0];
            $extension = $arrFile[1];
            if($extension == "txt") {
                $res = ["success"=>true, "message"=>"File is correct"];
            } else {
                $res = ["success"=>false, "message"=>"Please upload a text file only"];
            }
        } else {
            $res = ["success"=>false, "message"=>"Please upload a valid file"];
        }
        return json_encode($res);
    }
}
?>
